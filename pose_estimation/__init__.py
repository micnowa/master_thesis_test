import json
import os

import yaml

import utils


def save_to_json(json_path, json_file_name, json_data):
    json_file = json.dumps(json_data)
    output_full_path = os.path.join(json_path, json_file_name)
    f = open(output_full_path, "w")
    f.write(json_file)
    f.close()


if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    with open(dir_path + "/resources/available_actions.yaml") as ymlfile:
        available_actions = yaml.safe_load(ymlfile)
        available_actions = available_actions['actions']

    UCF_101_pose_diff = cfg['UCF-101-pose-diff']
    UCF_101_fail_pose_diff = cfg['UCF-101-fail-pose-diff']

    train_set_action_files_names = utils.get_files_in_directory(UCF_101_pose_diff)
    test_set_action_files_names = utils.get_files_in_directory(UCF_101_fail_pose_diff)

    train_file_full_path_dict = {}
    test_file_full_path_dict = {}
    train_json_dict = {}
    test_json_dict = {}

    for action in available_actions:
        train_action_string = '_{}_'.format(action)
        train_set_action_data = [item for item in train_set_action_files_names if train_action_string in item]
        test_set_action_data = [item for item in test_set_action_files_names if action in item]

        # TRAIN
        for json_data_file_name in train_set_action_data:
            if action not in train_file_full_path_dict:
                train_file_full_path_dict[action] = []
                train_json_dict[action] = []
            json_path = os.path.join(UCF_101_pose_diff, json_data_file_name)
            train_file_full_path_dict[action].append(json_path)
            with open(json_path) as json_file:
                json_data = json.load(json_file)
                train_json_dict[action].append(json_data)
        # TEST
        for json_data_file_name in test_set_action_data:
            if action not in test_file_full_path_dict:
                test_file_full_path_dict[action] = []
                test_json_dict[action] = []
            json_path = os.path.join(UCF_101_fail_pose_diff, json_data_file_name)
            test_file_full_path_dict[action].append(json_path)
            with open(json_path) as json_file:
                json_data = json.load(json_file)
                test_json_dict[action].append(json_data)
    print('Loading done!')

    # Real comparison
    train_detected_ratio = {}
    test_detected_ratio = {}
    for action in available_actions:
        train_action_list = train_json_dict[action]
        test_action_list = test_json_dict[action]
        train_detected_ratio[action] = 0
        test_detected_ratio[action] = 0
        total_frames_for_action = 0
        # Frames for which pose was not detected:
        for video_lists in train_action_list:
            for frame_entry in video_lists:
                if not video_lists[frame_entry]:
                    train_detected_ratio[action] = train_detected_ratio[action] + 1
                total_frames_for_action = total_frames_for_action + 1
        train_detected_ratio[action] = train_detected_ratio[action] / total_frames_for_action

        total_frames_for_action = 0
        for video_lists in test_action_list:
            for frame_entry in video_lists:
                if not video_lists[frame_entry]:
                    test_detected_ratio[action] = test_detected_ratio[action] + 1
                total_frames_for_action = total_frames_for_action + 1
        test_detected_ratio[action] = test_detected_ratio[action] / total_frames_for_action

        print('a')
    save_to_json(json_path=cfg['UCF-101-fail-pose-result'],
                 json_file_name='pose_miss_ratio_train.json',
                 json_data=test_detected_ratio)

    save_to_json(json_path=cfg['UCF-101-fail-pose-result'],
                 json_file_name='pose_miss_ratio_test.json',
                 json_data=train_detected_ratio)
    print('b')
