import os.path

import cv2
import yaml
import tensorflow as tf

from neural_networks.i3d import i3d

classified_sample = 'v_SumoWrestling_g01_c04'


def classify(path):
    if os.path.isfile(path):
        print('Processing ...')

    else:
        print('File doesn\'t exist')
        raise FileExistsError('File doesn\t exist ...')


def get_length(video_name):
    cap = cv2.VideoCapture(video_name)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    return frame_count / fps


yaml_name = 'i3d_config.yaml'
dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "\\resources\\" + yaml_name) as ymlfile:
    cfg = yaml.safe_load(ymlfile)

file_name = cfg['ucf_path'] + '\\SumoWrestling\\' + classified_sample
print(file_name)

with open(cfg['kinetics_classes']) as ymlfile:
    classes_dict = yaml.safe_load(ymlfile)

kinetics_classes = classes_dict.keys()
print(len(kinetics_classes))

rgb_input = tf.placeholder(
    tf.float32,
    shape=(1, get_length(file_name), 244, 244, 3))

with tf.variable_scope('RGB'):
    rgb_model = i3d.InceptionI3d(
        400, spatial_squeeze=True, final_endpoint='Logits')
    rgb_logits, _ = rgb_model(
        rgb_input, is_training=False, dropout_keep_prob=1.0)

rgb_variable_map = {}
for variable in tf.global_variables():

    if variable.name.split('/')[0] == 'RGB':
        rgb_variable_map[variable.name.replace(':0', '')] = variable

rgb_saver = tf.train.Saver(var_list=rgb_variable_map, reshape=True)
