import getopt
import os
import sys

import yaml

dir_path = os.path.dirname(os.path.realpath(__file__))


def load_checkpoints_dict():
    # Read yaml file
    with open(dir_path + "/i3d/resources/i3d_config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    checkpoints = cfg['_CHECKPOINTS']
    for x in checkpoints:
        check = cfg['_CHECKPOINTS_PATH'] + checkpoints[x]
        checkpoints[x] = check
    return checkpoints


def opt_dict_read(sys_args):
    dict = {}
    try:
        opts, args = getopt.getopt(sys_args, "i:s:r:f:h", ["img_size=", "sample_video_frames=", "rgb=", "flow=", "help"])
    except getopt.GetoptError:
        print('Bad parsing ...')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            sys.exit(2)
        elif opt in ("-i", "--img_size"):
            dict['img_size'] = arg
        elif opt in ("-s", "--sample_video_frames"):
            dict['sample_video_frames'] = arg
        elif opt in ("-r", "--rgb"):
            dict['rgb'] = arg
        elif opt in ("-f", "--flow"):
            dict['flow'] = arg
    return dict

