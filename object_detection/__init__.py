import json
import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import yaml
import processing as pandas_processing
import utils
import getopt


def save_to_json(json_path, json_file_name, json_data):
    json_file = json.dumps(json_data)
    output_full_path = os.path.join(json_path, json_file_name)
    f = open(output_full_path, "w")
    f.write(json_file)
    f.close()


if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    with open(dir_path + "/resources/available_actions.yaml") as ymlfile:
        available_actions = yaml.safe_load(ymlfile)
        available_actions = available_actions['actions']

    UCF_101_objects_diff_path = cfg['UCF-101-objects-json-diff']
    UCF_101_fail_objects_diff_path = cfg['UCF-101-fail-objects-json-diff']

    UCF_101_objects_json_analysed = cfg['UCF-101-objects-json-analysed']
    UCF_101_fail_objects_json_analysed = cfg['UCF-101-fail-objects-json-analysed']

    UCF_101_pose_diff = cfg['UCF-101-pose-diff']
    UCF_101_fail_pose_diff = cfg['UCF-101-fail-pose-diff']

    train_json_output_dir = cfg['UCF-101-objects-json-analysed']
    test_json_output_dir = cfg['UCF-101-fail-objects-json-analysed']
    print(available_actions)

    process_training_set = False
    options, remainder = getopt.getopt(sys.argv[1:], 'p:t:c:a:v:e:', [
        'prob=',
        'test=',
        'train=',
        'compare=',
        'visualize=',
        'analyse=',
        'estimate='
    ])
    process_test_set = False
    compare_sets = False
    analyse_comparison = False
    visualize_comparison = False
    estimate_pose = False
    prob = 0

    for opt, arg in options:
        if opt in ('-t', '--train'):
            if arg == 'true':
                process_training_set = True

        if opt in ('-T', '--test'):
            if arg == 'true':
                process_test_set = True

        if opt in ('-p', '--prob'):
            arg = arg.replace('=', '')
            prob = float(arg)

        if opt in ('-c', '--compare'):
            if arg == 'true':
                compare_sets = True

        if opt in ('-a', '--analyse'):
            if arg == 'true':
                analyse_comparison = True

        if opt in ('-v', '--visualize'):
            if arg == 'true':
                visualize_comparison = True

        if opt in ('-e', '--estimate'):
            if arg == 'true':
                estimate_pose = True

    if process_training_set is True:
        for file_name in utils.get_files_in_directory(UCF_101_objects_diff_path):
            file_full_path = os.path.join(UCF_101_objects_diff_path, file_name)
            for action_name in available_actions:
                if action_name in file_name:
                    print(file_name)
                    print(file_full_path)
                    # Processing
                    pandas_json = pandas_processing.process_object_detection_db(json_path=file_full_path,
                                                                                prob=prob)
                    json_file = json.dumps(pandas_json)
                    output_full_path = os.path.join(train_json_output_dir, file_name.replace('-diff', ''))
                    f = open(output_full_path, "w")
                    f.write(json_file)
                    f.close()

    if process_test_set is True:
        for file_name in utils.get_files_in_directory(UCF_101_fail_objects_diff_path):
            file_full_path = os.path.join(UCF_101_fail_objects_diff_path, file_name)
            print(file_name)
            print(file_full_path)
            # Processing
            pandas_json = pandas_processing.process_object_detection_db(json_path=file_full_path,
                                                                        prob=prob)
            json_file = json.dumps(pandas_json)
            output_full_path = os.path.join(test_json_output_dir, file_name.replace('-diff', ''))
            f = open(output_full_path, "w")
            f.write(json_file)
            f.close()

    if compare_sets is True:
        train_set_action_files_names = utils.get_files_in_directory(UCF_101_objects_json_analysed)
        test_set_action_files_names = utils.get_files_in_directory(UCF_101_fail_objects_json_analysed)
        train_file_full_path_dict = {}
        test_file_full_path_dict = {}

        train_json_dict = {}
        test_json_dict = {}

        for action in available_actions:
            train_action_string = '_{}_'.format(action)
            train_set_action_data = [item for item in train_set_action_files_names if train_action_string in item]
            test_set_action_data = [item for item in test_set_action_files_names if action in item]
            # Load json data for actions
            # TRAIN
            for json_data_file_name in train_set_action_data:
                if action not in train_file_full_path_dict:
                    train_file_full_path_dict[action] = []
                    train_json_dict[action] = []
                json_path = os.path.join(UCF_101_objects_json_analysed, json_data_file_name)
                train_file_full_path_dict[action].append(json_path)
                with open(json_path) as json_file:
                    json_data = json.load(json_file)
                    train_json_dict[action].append(json_data)
            # TEST
            for json_data_file_name in test_set_action_data:
                if action not in test_file_full_path_dict:
                    test_file_full_path_dict[action] = []
                    test_json_dict[action] = []
                json_path = os.path.join(UCF_101_fail_objects_json_analysed, json_data_file_name)
                test_file_full_path_dict[action].append(json_path)
                with open(json_path) as json_file:
                    json_data = json.load(json_file)
                    test_json_dict[action].append(json_data)
        print('whatsoever')

        # Comparing train and test datasets
        comparison_result_dict = {}
        for action_name in train_json_dict:
            print(action_name)
            comparison_dict = pandas_processing.compare_actions(train_json=train_json_dict[action_name],
                                                                test_json=test_json_dict[action_name])
            comparison_result_dict[action_name] = comparison_dict
        json_file = json.dumps(comparison_result_dict)
        output_full_path = os.path.join(test_json_output_dir,
                                        os.path.join(cfg['UCF-101-objects-compare-results'], 'object_compare.json'))
        f = open(output_full_path, "w")
        f.write(json_file)
        f.close()

    if analyse_comparison is True:
        analysed_path = os.path.join(cfg['UCF-101-objects-compare-results'], 'object_compare.json')
        with open(analysed_path) as json_file:
            detected_objects_dict_coverage = json.load(json_file)

        total_comparisons = 0
        object_coverage_dict = {}
        for action_name in detected_objects_dict_coverage:
            action_list = detected_objects_dict_coverage[action_name]
            for object_name in action_list:
                if object_name not in object_coverage_dict:
                    object_coverage_dict[object_name] = []
                coverage_list = action_list[object_name]
                for coverage_value in coverage_list:
                    object_coverage_dict[object_name].append(coverage_value)

        object_coverage_dict_lengths = {}
        object_coverage_dict_average = {}
        for object_name in object_coverage_dict:
            object_coverage_dict_lengths[object_name] = len(object_coverage_dict[object_name])
            object_coverage_dict_average[object_name] = sum(object_coverage_dict[object_name]) / len(
                object_coverage_dict[object_name])

        save_to_json(json_path=cfg['UCF-101-objects-compare-results'],
                     json_file_name='object_compare_analysed.json',
                     json_data=object_coverage_dict)
        save_to_json(json_path=cfg['UCF-101-objects-compare-results'],
                     json_file_name='object_compare_analysed_lengths.json',
                     json_data=object_coverage_dict_lengths)
        save_to_json(json_path=cfg['UCF-101-objects-compare-results'],
                     json_file_name='object_compare_analysed_averages.json',
                     json_data=object_coverage_dict_average)

    if visualize_comparison is True:
        data_path = os.path.join(cfg['UCF-101-objects-compare-results'], 'object_compare_analysed.json')
        average_path = os.path.join(cfg['UCF-101-objects-compare-results'], 'object_compare_analysed_averages.json')
        length_path = os.path.join(cfg['UCF-101-objects-compare-results'], 'object_compare_analysed_lengths.json')

        with open(data_path) as json_file:
            data_dict = json.load(json_file)
        with open(average_path) as json_file:
            average_dict = json.load(json_file)
        with open(length_path) as json_file:
            length_dict = json.load(json_file)

        # Person histogram
        person_table = data_dict['person']
        person_table.sort()
        person_table = pd.DataFrame(person_table)
        ax = person_table.plot.hist(bins=10)
        ax.set_xlabel('coverage rate')
        ax.set_ylabel('number of appearances')
        ax.set_title('Coverage histogram for person object')
        ax.get_legend().remove()
        fig = ax.get_figure()
        save_path = os.path.join(cfg['UCF-101-objects-compare-results'], 'person_hist.pdf')
        fig.savefig(save_path)
        plt.show()

        sorted_length_dict = {}
        for k in sorted(average_dict, key=average_dict.get, reverse=True):
            print(k, average_dict[k])
