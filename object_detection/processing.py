import json
import math
import pandas as pd


def process_object_detection_db(json_path, prob):
    with open(json_path) as file:
        json_data = json.load(file)

    pandas_data = pd.DataFrame(json_data)
    pandas_data = pandas_data.transpose()

    pandas_data = pre_processing(pandas_data)
    pandas_data = remove_outliers_objects(pandas_data, prob)
    object_data_frames = get_objects_dataframes(pandas_data)
    return count_object_statistics(object_data_frames)


def pre_processing(pandas_data):
    pandas_data = pandas_data[~pandas_data['d_frame'].isin([0])]
    pandas_data = pandas_data.set_axis(pd.Index([i for i in range(len(pandas_data.axes[0]))]), axis='index')
    pandas_data['dxdy'] = (pandas_data.d_x * pandas_data.d_x + pandas_data.d_y * pandas_data.d_y).apply(math.sqrt)
    pandas_data.d_prob = pandas_data.d_prob / 100
    del pandas_data['d_x']
    del pandas_data['d_y']
    return pandas_data


def remove_outliers_objects(pandas_data, prob):
    rows_threshold = int(float(prob) * len(pandas_data.index))
    objects_names = pandas_data.name.unique()
    for object_name in objects_names:
        print(object_name)
        rows_count = len(pandas_data.loc[pandas_data['name'] == object_name].index)
        if rows_count < rows_threshold:
            pandas_data = pandas_data[pandas_data['name'] != object_name]
    return pandas_data


def get_objects_dataframes(pandas_data):
    objects_dataframes_dict = {}
    objects_names = pandas_data.name.unique()
    for object_name in objects_names:
        objects_dataframes_dict[object_name] = pandas_data[pandas_data['name'] == object_name].drop(['name'],
                                                                                                    axis='columns')
        objects_dataframes_dict[object_name] = objects_dataframes_dict[object_name].drop(['d_prob'], axis='columns')
    return objects_dataframes_dict


def count_object_statistics(object_dataframe_dict):
    statistics_dict = {}
    for entry in object_dataframe_dict:
        pandas_data = object_dataframe_dict[entry]
        mean_entry = pandas_data.mean(axis=0)
        std_entry = pandas_data.std(axis=0)
        max_entry = pandas_data.max(axis=0)
        min_entry = pandas_data.min(axis=0)
        statistics_dict[entry] = json.loads(
            pd.DataFrame([mean_entry, std_entry, max_entry, min_entry]).transpose().rename(
                columns={0: 'mean', 1: 'std', 2: 'max', 3: 'min'}).to_json())
    return statistics_dict


def compare_actions(train_json, test_json):
    common_objects_set = extract_common_objects(train_json, test_json)
    train_object_dict = common_objects_to_dict(json_data=train_json,
                                               common_objects_set=common_objects_set)
    test_object_dict = common_objects_to_dict(json_data=test_json,
                                              common_objects_set=common_objects_set)
    comparison_dict = {}
    # for object_name in train_json:
    #     comparison_dict[object_name] = []
    #     train_objects = train_object_dict[object_name]
    #     test_objects = test_object_dict[object_name]
    #     comparison_dict[object_name].append(compare_video_objects(train_objects=train_objects,
    #                                                           test_objects=test_objects))
    for (k, train_objects), (k2, test_objects) in zip(train_object_dict.items(), test_object_dict.items()):
        print(k)
        comparison_dict[k] = []
        comparison_dict[k].append(compare_video_objects(train_objects=train_objects, test_objects=test_objects))
    return comparison_dict


def compare_video_objects(train_objects, test_objects):
    train_statistics = gather_statistics_from_video_objects(train_objects)
    test_statistics = gather_statistics_from_video_objects(test_objects)
    border_points = {}
    column_number = len(train_statistics[0])
    for i in range(0, column_number):
        if i not in border_points:
            border_points[i] = [float('inf'), float('-inf')]
        for numbers_list_index in train_statistics:
            numbers_list = train_statistics[numbers_list_index]
            number = numbers_list[i]
            border_points_index_data = border_points[i]
            if number is None:
                continue
            if number < border_points_index_data[0]:
                border_points_index_data[0] = number
            if number > border_points_index_data[1]:
                border_points_index_data[1] = number
            border_points[i] = border_points_index_data

    in_range_points = {}
    for numbers_list_index in test_statistics:
        numbers_list = test_statistics[numbers_list_index]
        in_range_points[numbers_list_index] = 0
        for i in range(0, column_number):
            number = numbers_list[i]
            border_points_for_index = border_points[numbers_list_index]
            min_number = border_points_for_index[0]
            max_number = border_points_for_index[1]
            if number is not None:
                if min_number <= number <= max_number:
                    in_range_points[numbers_list_index] = in_range_points[numbers_list_index] + 1
        in_range_points[numbers_list_index] = in_range_points[numbers_list_index] / column_number
    return in_range_points


def gather_statistics_from_video_objects(video_objects):
    video_objects_dictionary = {}
    i = 0
    for dictionary in video_objects:
        video_objects_dictionary[i] = []
        for statistic_measure in dictionary:
            measure = dictionary[statistic_measure]
            for static_measure_detail in measure:
                statistic_measure_value = measure[static_measure_detail]
                video_objects_dictionary[i].append(statistic_measure_value)
        i = i + 1
    return video_objects_dictionary


def extract_common_objects(train_json, test_json):
    object_dataset_test = set()
    object_dataset_train = set()
    for entry in test_json:
        for key in entry:
            object_dataset_test.add(key)
    for entry in train_json:
        for key in entry:
            object_dataset_train.add(key)
    return object_dataset_train.intersection(object_dataset_test)


def common_objects_to_dict(json_data, common_objects_set):
    objects_dict = {}
    for video_objects in json_data:
        for detected_object in video_objects:
            if detected_object not in common_objects_set:
                continue
            print(video_objects[detected_object])
            if detected_object not in objects_dict:
                objects_dict[detected_object] = []
            objects_dict[detected_object].append(video_objects[detected_object])
    return objects_dict
